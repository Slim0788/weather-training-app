package com.vironit.weathertrainingapp;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkService {

    private static final String BRS_BASE_URL = "http://ais.brs.by:38516/ords/mobile_user/v1_3/";
    private static NetworkService instance;
    private Retrofit retrofit;

    private NetworkService (){

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder client = new OkHttpClient.Builder()
                .addInterceptor(interceptor);

        retrofit = new Retrofit.Builder()
                .baseUrl(BRS_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build();
    }

    public static NetworkService getInstance() {
        if (instance == null){
            instance = new NetworkService();
        }
        return instance;
    }

    public WeatherAPI getBRSApi(){
        return retrofit.create(WeatherAPI.class);
    }
}
