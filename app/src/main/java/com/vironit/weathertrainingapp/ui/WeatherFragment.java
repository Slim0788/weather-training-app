package com.vironit.weathertrainingapp.ui;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.vironit.weathertrainingapp.App;
import com.vironit.weathertrainingapp.R;
import com.vironit.weathertrainingapp.WeatherAPI;
import com.vironit.weathertrainingapp.pojo.weather.WeatherResponse;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherFragment extends Fragment {
    private final String TAG = "WEATHER";
    public static String APP_ID = "82bf1ae11d0d79169c84285f5ffb9057";
    public static String units = "metric";

    private TextView tv_temperature;
    private TextView tv_humidity;
    private TextView tv_pressure;
    private TextView tv_wind;
    private ImageView iv_image;
    private ProgressBar progressBar;
    private SearchView searchView;

    private String text;

    @Inject
    WeatherAPI weatherAPI;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        App.buildComponent();
        App.appComponent().inject(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weather, container, false);

        setHasOptionsMenu(true);

        tv_humidity = view.findViewById(R.id.tv_humidity);
        tv_wind = view.findViewById(R.id.tv_wind);
        tv_temperature = view.findViewById(R.id.tv_temperature);
        tv_pressure = view.findViewById(R.id.tv_pressure);
        iv_image = view.findViewById(R.id.iv_image);
        progressBar = view.findViewById(R.id.progressBar);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu, menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) menuItem.getActionView();
        searchView.setQueryHint("Enter city");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                text = s;
                progressBar.setVisibility(ProgressBar.VISIBLE);
                weatherDetails();
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });


    }

    public void weatherDetails() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Call call = weatherAPI.getWeatherByCity(text, units, APP_ID);

                call.enqueue(new Callback() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(Call call, Response response) {
                        if (response.isSuccessful()) {

                            Log.d(TAG, "onResponse");

                            if (response.body() != null) {
                                WeatherResponse weatherResponse = (WeatherResponse) response.body();

                                Glide.with(getActivity().getApplication())
                                        .load("http://openweathermap.org/img/w/" + weatherResponse.getWeather().get(0).getIcon() + ".png")
                                        .into(iv_image);
                                tv_temperature.setText(weatherResponse.getMain().getTemp() + " °C");
                                tv_wind.setText("Ветер " + weatherResponse.getWind().getSpeed().toString() + " м/с");
                                tv_humidity.setText("Влажность " + weatherResponse.getMain().getHumidity() + " %");
                                tv_pressure.setText("Давление " + weatherResponse.getMain().getPressure() + " гПа");

                                progressBar.setVisibility(ProgressBar.INVISIBLE);
                            }
                        } else {
                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                            Log.d(TAG, "response code error: " + response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call call, Throwable t) {
                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                        Log.d(TAG, "onFailure");
                        Log.d(TAG, t.toString());
                    }
                });
            }
        }).start();
    }

}
