package com.vironit.weathertrainingapp.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.SearchView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterManager;
import com.vironit.weathertrainingapp.map.ClusterItemRendered;
import com.vironit.weathertrainingapp.map.MyItem;
import com.vironit.weathertrainingapp.NetworkService;
import com.vironit.weathertrainingapp.R;
import com.vironit.weathertrainingapp.pojo.belrosstrah.BelrosstrahResponse;
import com.vironit.weathertrainingapp.pojo.belrosstrah.Item;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapFragment extends Fragment implements OnMapReadyCallback {

    private static final String TAG = "MAP";

    private MapView mapView;
    private GoogleMap gMap;
    private ClusterManager<MyItem> clusterManager;
    private List<Item> items = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);

        mapView = view.findViewById(R.id.map);
        mapView.getMapAsync(this);
        mapView.onCreate(savedInstanceState);
        Log.d(TAG, "onCreateView: ");

        return view;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;

        LatLng Minsk = new LatLng(53.88, 27.58);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(Minsk)
                .zoom(5.8f)
                .build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        gMap.animateCamera(cameraUpdate);

        NetworkService.getInstance()
                .getBRSApi()
                .getBelrosstrahOffices()
                .enqueue(new Callback<BelrosstrahResponse>() {
                    @Override
                    public void onResponse(Call<BelrosstrahResponse> call, Response<BelrosstrahResponse> response) {
                        Log.d(TAG, "onResponse");

                        BelrosstrahResponse belrosstrahResponse = response.body();
                        items = belrosstrahResponse.getItems();

                        setUpClasterer();
                    }

                    @Override
                    public void onFailure(Call<BelrosstrahResponse> call, Throwable t) {
                        Log.d(TAG, "onFailure");
                        Log.d(TAG, t.toString());
                        t.printStackTrace();
                    }
                });
    }

    private void setUpClasterer() {

        clusterManager = new ClusterManager<MyItem>(getActivity().getApplicationContext(), gMap);

        gMap.setOnCameraIdleListener(clusterManager);
        gMap.setOnMarkerClickListener(clusterManager);

        addItems();
    }

    private void addItems() {

        for (Item item : items) {
            MyItem myItem = new MyItem(item.getLatitude(), item.getLongitude(), item.getId().toString(), item.getAddress(), item.getPhone());

            clusterManager.addItem(myItem);
        }
        clusterManager.setRenderer(new ClusterItemRendered(getContext(), gMap, clusterManager));
        clusterManager.cluster();
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume: ");
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onStart() {
        Log.d(TAG, "onStart: ");
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop: ");
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause: ");
        mapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy: ");
        mapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        Log.d(TAG, "onLowMemory: ");
        super.onLowMemory();
        mapView.onLowMemory();
    }

}
