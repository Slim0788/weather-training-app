package com.vironit.weathertrainingapp.dagger;

import com.vironit.weathertrainingapp.App;
import com.vironit.weathertrainingapp.ui.WeatherFragment;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;

@Singleton
@Component(modules = {AppModule.class, WeatherAPIModule.class})
public interface AppComponent extends AndroidInjector {

    void inject(WeatherFragment weatherFragment);

    final class Initializer {
        private Initializer() {
        }

        public static AppComponent init(App app) {
            return DaggerAppComponent
                    .builder()
                    .appModule(new AppModule(app))
                    .build();
        }
    }
}