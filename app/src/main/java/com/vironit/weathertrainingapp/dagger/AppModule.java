package com.vironit.weathertrainingapp.dagger;

import android.app.Application;

import com.vironit.weathertrainingapp.App;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    private App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides
    @Singleton
    protected Application provideApp() {
        return app;
    }
}
