package com.vironit.weathertrainingapp;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.vironit.weathertrainingapp.ui.MapFragment;
import com.vironit.weathertrainingapp.ui.WeatherFragment;

public class PageAdapter extends FragmentPagerAdapter {

    private String[] pageName = {"Weather", "Map"};
    private static int ITEM_COUNT = 2;

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return pageName[position];
    }

    public PageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                WeatherFragment weatherFragment = new WeatherFragment();
                return weatherFragment;
            case 1:
                MapFragment mapFragment = new MapFragment();
                return mapFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return ITEM_COUNT;
    }
}
