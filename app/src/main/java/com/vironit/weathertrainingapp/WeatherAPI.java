package com.vironit.weathertrainingapp;

import com.vironit.weathertrainingapp.pojo.belrosstrah.BelrosstrahResponse;
import com.vironit.weathertrainingapp.pojo.belrosstrah.Item;
import com.vironit.weathertrainingapp.pojo.weather.WeatherResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface WeatherAPI {
    @GET("data/2.5/weather?")
    Call<WeatherResponse> getWeatherByCity(
            @Query("q") String city,
            @Query("units") String units,
            @Query("APPID") String app_id);

    @GET("GetOfficesInfo/")
    Call<BelrosstrahResponse> getBelrosstrahOffices();
}
