package com.vironit.weathertrainingapp;

import android.app.Application;

import com.vironit.weathertrainingapp.dagger.AppComponent;

public class App extends Application {

    private static AppComponent appComponent;
    private static App app;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;

        buildComponent();
    }

    public static AppComponent appComponent() {
        return appComponent;
    }

    public static void buildComponent() {
        appComponent = AppComponent.Initializer.init(app);
    }
}
